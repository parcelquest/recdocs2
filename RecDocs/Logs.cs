﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace RecDocs
{
   public class LogWriter
   {
      private static LogWriter instance;
      private static Queue<Log> logQueue;
      private static string m_logDir = "H:\\CO_LOGS\\RecDocs";
      private static string logFile = "MyRecDocs.log"; 
      private static int maxLogAge = 1; //int.Parse(<Max Age in seconds or Config Setting>);
      private static int queueSize = 1; //int.Parse(<Max Queue Size or Config Setting);
      private static DateTime LastFlushed = DateTime.Now;

      /// <summary>
      /// Private constructor to prevent instance creation
      /// </summary>
      private LogWriter() 
      {
         m_logDir = AppDomain.CurrentDomain.BaseDirectory + AppDomain.CurrentDomain.RelativeSearchPath;
      }

      public static string GetFilenameYYYMMDD(string suffix, string extension)
      {
         return System.DateTime.Now.ToString("yyyy_MM_dd") + suffix + extension;
      }

      public static void WriteLog(String message)
      {
         //just in case: we protect code with try.
         try
         {
            string filename = m_logDir + GetFilenameYYYMMDD("_LOG", ".log");
            System.IO.StreamWriter sw = new System.IO.StreamWriter(filename, true);
            XElement xmlEntry = new XElement("logEntry",
                new XElement("Date", System.DateTime.Now.ToString()),
                new XElement("Message", message));
            sw.WriteLine(xmlEntry);
            sw.Close();
         }
         catch (Exception)
         {
         }
      }


      /// <summary>
      /// An LogWriter instance that exposes a single instance
      /// </summary>
      public static LogWriter Instance
      {
         get
         {
               // If the instance is null then create one and init the Queue
               if (instance == null)
               {
                  instance = new LogWriter();
                  logQueue = new Queue<Log>();
               }
               return instance;
         }
      }

      /// <summary>
      /// The single instance method that writes to the log file
      /// </summary>
      /// <param name="message">The message to write to the log</param>
      public void WriteToLog(string message)
      {
         // Lock the queue while writing to prevent contention for the log file
         lock (logQueue)
         {
               // Create the entry and push to the Queue
               Log logEntry = new Log(message);
               logQueue.Enqueue(logEntry);

               // If we have reached the Queue Size then flush the Queue
               if (logQueue.Count >= queueSize || DoPeriodicFlush())
               {
                  FlushLog();
               }
         }            
      }

      private bool DoPeriodicFlush()
      {
         TimeSpan logAge = DateTime.Now - LastFlushed;
         if (logAge.TotalSeconds >= maxLogAge)
         {
               LastFlushed = DateTime.Now;
               return true;
         }
         else
         {
               return false;
         }
      }

      /// <summary>
      /// Flushes the Queue to the physical log file
      /// </summary>
      private void FlushLog()
      {
         while (logQueue.Count > 0)
         {
               Log entry = logQueue.Dequeue();
               string logPath = logDir + entry.LogDate + "_" + logFile;

               // This could be optimised to prevent opening and closing the file for each write
               using (FileStream fs = File.Open(logPath, FileMode.Append, FileAccess.Write))
               {
                  using (StreamWriter log = new StreamWriter(fs))
                  {
                     log.WriteLine(string.Format("{0}\t{1}",entry.LogTime,entry.Message));
                  }
               }
         }            
      }
   }

   /// <summary>
   /// A Log class to store the message and the Date and Time the log entry was created
   /// </summary>
   public class Log
   {
      public string Message { get; set; }
      public string LogTime { get; set; }
      public string LogDate { get; set; }

      public Log(string message)
      {
         Message = message;
         LogDate = DateTime.Now.ToString("yyyy-MM-dd");
         LogTime = DateTime.Now.ToString("hh:mm:ss.fff tt");
      }
   }
}
