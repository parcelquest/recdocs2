﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Configuration;

namespace RecDocs
{
   public partial class RecDocs : Form
   {
      private string _fileName = "";
      private bool b_modified = false;
      private bool b_silent = false;
      private XDocument _doc;
      private DataTable dttemp = new DataTable();
      DataSet ds = new DataSet();
      private BindingSource bindingSource1 = new BindingSource();
      private string _logFile = "";
      private string _logDir = Properties.Settings.Default.LogDir;
      private string sCnty = "";
      public static string GetFilenameYYYMMDD(string prefix, string suffix, string extension)
      {
         return prefix + System.DateTime.Now.ToString("yyyyMMdd") + suffix + extension;
      }

      public void LogMsg(string message)
      {
         if (_logFile == "")
            _logFile = _logDir + GetFilenameYYYMMDD(sCnty+"_", "", ".log");
         StreamWriter sw = new StreamWriter(_logFile, true);
         sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ' ' + message);
         sw.Flush();
         sw.Close();
      }
      
      public RecDocs(string[] args)
      {
         InitializeComponent();
         ceTe.DynamicPDF.Document.AddLicense("GEN70NEDOKLDAJJqsKvcVsJVwAhfvsYQmUyPt9qqbNZrz+CtMn434ZvyyKqQ5wvs1527jFC8sNBZmznG52KtclBAQeegj5SgL5/A");

         string sMsg = "Recdocs";
         Int16 i;

         for (i = 0; i < args.Length; i++)
         {
            sMsg += " " + args[i];
            if (args[i] == "-C")
            {
               sCnty = args[++i];
               sMsg += " " + args[i];
            }
         }

         LogMsg(sMsg);
         createNewXMLDoc();
         processArgs(args);
      }

      public RecDocs()
      {
         InitializeComponent();
         ceTe.DynamicPDF.Document.AddLicense("GEN70NEDOKLDAJJqsKvcVsJVwAhfvsYQmUyPt9qqbNZrz+CtMn434ZvyyKqQ5wvs1527jFC8sNBZmznG52KtclBAQeegj5SgL5/A");
         createNewXMLDoc();
      }

      #region MenuActions
      private void newToolStripMenuItem_Click(object sender, EventArgs e)
      {
         DialogResult result = DialogResult.No;
         if (b_modified == true)
         {
               result = MessageBox.Show("Save changes to current file first?","Save Changes?",MessageBoxButtons.YesNoCancel);
         }
         if (result == DialogResult.Yes)
         {
               // Save the modified file.
               saveToolStripMenuItem_Click(sender, e);
         }
         if (result != DialogResult.Cancel)
         {
               createNewXMLDoc();
         }
      }
      private void openToolStripMenuItem_Click(object sender, EventArgs e)
      {
         DialogResult resultSave = DialogResult.No;
         if (b_modified == true)
         {
               resultSave = MessageBox.Show("Save changes to current file first?", "Save Changes?", MessageBoxButtons.YesNoCancel);
         }
         if (resultSave == DialogResult.Yes)
         {
               // Save the modified file.
               saveToolStripMenuItem_Click(sender, e);
         }
         if (resultSave != DialogResult.Cancel)
         {
            openFileDialog1.Filter = "XML Files|*.xml|All Files|*.*";
            openFileDialog1.FilterIndex = 1;
            DialogResult result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
               try
               {
                  _fileName = openFileDialog1.FileName;
                  _doc = XDocument.Load(_fileName);
                  readFields();
                  b_modified = false;
                  updateFilename();
                  getData();
               }
               catch (Exception ex)
               {
                  MessageBox.Show("Error loading " + _fileName + ". " + ex.Message);
               }
            }
         }
      }
      private void openXmlDoc(string _fileName)
      {
         try
         {
            _doc = XDocument.Load(_fileName);
            readFields();
            b_modified = false;
            updateFilename();
            getData();
         }
         catch (Exception ex)
         {
            MessageBox.Show("Error loading " + _fileName + ". " + ex.Message);
         }
      }
      private void importToolStripMenuItem_Click(object sender, EventArgs e)
      {
         DialogResult result = DialogResult.No;
         if (b_modified == true)
         {
            result = MessageBox.Show("Save changes to current file first?", "Save Changes?", MessageBoxButtons.YesNoCancel);
         }
         if (result == DialogResult.Yes)
         {
            // Save the modified file.
            saveToolStripMenuItem_Click(sender, e);
         }
         if (result != DialogResult.Cancel)
         {
            openFileDialog1.Filter = "RDS Files |*.rds";
            openFileDialog1.FilterIndex = 1;
            DialogResult resultOpen = openFileDialog1.ShowDialog();

            if (resultOpen == DialogResult.OK)
            {
               readRDS(openFileDialog1.FileName);
               getData();
            }
         }
      }
      private void saveToolStripMenuItem_Click(object sender, EventArgs e)
      {
         if ((_fileName != "") && (b_modified == true))
         {
            FileInfo fi = new FileInfo(_fileName);
            if (!fi.Directory.Exists)
            {
               MessageBox.Show("saveToolStripMenuItem_Click: Invalid path.");
               return;
            }
            saveFile();
         }
      }
      private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
      {
         saveFileDialog1.Filter = "XML Files|*.xml|All Files|*.*";
         saveFileDialog1.FilterIndex = 1;
         DialogResult result = saveFileDialog1.ShowDialog();

         if (result == DialogResult.OK)
         {
            try
            {
               FileInfo fi = new FileInfo(saveFileDialog1.FileName);
               if (!fi.Directory.Exists)
               {
                  MessageBox.Show("Directory doesn't exist.");
                  return;
               }
               if (fi.Exists)
               {
                  DialogResult resultOvr = MessageBox.Show("File " + saveFileDialog1.FileName + " exists.  Overwrite?", "Overwrite?", MessageBoxButtons.YesNoCancel);
                  if (resultOvr == DialogResult.No)
                  {
                        saveAsToolStripMenuItem_Click(sender, e);
                  }
                  else
                  {
                     if (resultOvr == DialogResult.Yes)
                     {
                        fi.Delete();
                        _fileName = saveFileDialog1.FileName;
                        saveFile();
                        b_modified = false;
                        updateFilename();
                     }
                  }
               }
               else
               {
                  _fileName = saveFileDialog1.FileName;
                  saveFile();
                  b_modified = false;
                  updateFilename();
               }
            }
            catch (Exception ex)
            {
               MessageBox.Show("Error loading " + _fileName + ". " + ex.Message);
            }
         }
      }
      private void exitToolStripMenuItem_Click(object sender, EventArgs e)
      {
         Application.Exit();
      }
      private void btnSrcBrowse_Click(object sender, EventArgs e)
      {
         folderBrowserDialog1.SelectedPath = txtSource.Text;
         DialogResult result = folderBrowserDialog1.ShowDialog();
         if (result == DialogResult.OK)
         {
            txtSource.Text = folderBrowserDialog1.SelectedPath;
         }
      }
      private void btnDestBrowse_Click(object sender, EventArgs e)
      {
         folderBrowserDialog1.SelectedPath = txtDest.Text;
         DialogResult result = folderBrowserDialog1.ShowDialog();
         if (result == DialogResult.OK)
         {
            txtDest.Text = folderBrowserDialog1.SelectedPath;
         }
      }
      #endregion

      #region FieldChanges
      private void fileChanged()
      {
         b_modified = true;
         updateFilename();
      }
      private void updateSettingNode(string nodeName, string newText)
      {
         try
         {
            if (_doc.Descendants(nodeName).Any() == false) // Add in missing element
            {
               _doc.Element("root").Add(new XElement(nodeName, newText));
            }
            else
            { // modify existing element.
               (from el in _doc.Descendants(nodeName) select el).First().SetValue(newText);
            }
         }
         catch (Exception e)
         {
            LogMsg("***** Error in updateSettingNode(): " + e.Message);
         }
      }
      private string getSettingStringValue(string nodeName)
      {
         return (string)(from el in _doc.Descendants(nodeName) select el).First();
      }
      private bool getSettingBoolValue(string nodeName)
      {
         bool result = false;
         try
         {
               result = bool.Parse((string)(from el in _doc.Descendants(nodeName) select el).First());
         }
         catch (Exception e)
         {
            LogMsg("***** Error in getSettingBoolValue(): " + e.Message);
         }
         return result;
      }
      private void txtSource_TextChanged(object sender, EventArgs e)
      {
         updateSettingNode("source", txtSource.Text);
         fileChanged();
      }
      private void txtDest_TextChanged(object sender, EventArgs e)
      {
         updateSettingNode("destination", txtDest.Text);
         fileChanged();
      }
      private void txtInTemplate_TextChanged(object sender, EventArgs e)
      {
         updateSettingNode("intemplate", txtInTemplate.Text);
         fileChanged();
      }
      private void chkDelOrig_CheckedChanged(object sender, EventArgs e)
      {
         updateSettingNode("delorig",chkDelOrig.Checked.ToString());
         fileChanged();
      }
      private void chkMerge_CheckedChanged(object sender, EventArgs e)
      {
         updateSettingNode("mergetiff", chkMerge.Checked.ToString());
         chkPDF.Enabled = chkMerge.Checked;
         chkNoRotate.Enabled = chkMerge.Checked;
         fileChanged();
      }
      private void chkPDF_CheckedChanged(object sender, EventArgs e)
      {
         updateSettingNode("topdf", chkPDF.Checked.ToString());
         fileChanged();
      }
      private void chkNoRotate_CheckedChanged(object sender, EventArgs e)
      {
         updateSettingNode("norotate", chkNoRotate.Checked.ToString());
         fileChanged();
      }
      #endregion

      private void updateFilename()
      {
         string modified = "";
         if (b_modified == true)
         {
            modified = "*";
            saveToolStripMenuItem.Enabled = true;
         }
         else
         {
            saveToolStripMenuItem.Enabled = false;
         }
         this.Text = "RecDocs (" + _fileName + ")" + modified;
      }
      public DataTable XElementToDataTable(XElement element)
      {
         ds.ReadXml(new StringReader(element.ToString()));
         return ds.Tables[0];
      }
      private void getData()
      {
         try
         {
            dttemp.Rows.Clear();
            //dttemp = XElementToDataTable(_doc.Descendants("data").First());
            XElement dataEl = new XElement("data",(
               from el in _doc.Descendants("data").Elements("item")
               orderby (string) el.Element("index")
               select (XElement) el
               ));
            dttemp = XElementToDataTable(dataEl);
            bindingSource1.DataSource = dttemp;
            gvData.DataSource = bindingSource1;
            gvData.AutoResizeColumns();
            upDownBtnEnable();
         } catch (Exception e)
         {
            LogMsg("***** Error in getData(): " + e.Message);
         }
      }
      private void createNewXMLDoc()
      {
         populateBlankXMLDoc();
         _fileName = "untitled.xml";
         updateFilename();
      }
      private void populateBlankXMLDoc()
      {
         _doc = 
               new XDocument(
                  new XElement("root",
                     new XElement("source"),
                     new XElement("destination"),
                     new XElement("intemplate"),
                     new XElement("delorig","false"),
                     new XElement("mergetiff","false"),
                     new XElement("topdf","false"),
                     new XElement("debug","false"),
                     new XElement("data")
                  )
               );
         txtSource.Text = "";
         txtDest.Text = "";
         chkDelOrig.Checked = false;
         chkMerge.Checked = false;
      }
      private void saveFile()
      {
         _doc.Save(_fileName);
         b_modified = false;
         updateFilename();
      }

      private void readFields()
      {
         txtSource.Text = getSettingStringValue("source");
         txtDest.Text = getSettingStringValue("destination");
         txtInTemplate.Text = getSettingStringValue("intemplate");
         chkDelOrig.Checked = getSettingBoolValue("delorig");
         chkMerge.Checked = getSettingBoolValue("mergetiff");
         chkPDF.Checked = getSettingBoolValue("topdf");
         chkNoRotate.Checked = getSettingBoolValue("norotate");
      }

      private void processArgs(string[] args)
      {
         int i;
         bool doProcess = false;
         for (i = 0; i < args.Length; i++)
         {
            if (args[i] == "-i")
            {
               string filename = "";
               filename = args[i + 1];
               openXmlDoc(filename);
               i++;
            }
            if (args[i] == "-p")
            {
               doProcess = true;
               b_silent = true;
            }
         }
         if (doProcess)
         {
            //btnProcess_Click(null, null);
            i = myProcess();
            if (System.Windows.Forms.Application.MessageLoop)
            {
               // Use this since we are a WinForms app
               System.Windows.Forms.Application.Exit();
            }
            else
            {
               if (i > 0)
                  LogMsg("Job done successfully! ... ");

               // Use this since we are a console app
               System.Environment.Exit(1);
            }
         }
      }

      #region ImportRDS
      private void readRDS(string fileRDS)
      {
         TextReader tr = null;
         string s;
         createNewXMLDoc();
         _fileName = fileRDS.Substring(0, fileRDS.LastIndexOf('.')) + ".xml";

         try
         {
            FileInfo fi = new FileInfo(fileRDS);
            if (fi.Exists)
            {
               tr = fi.OpenText();
               while ((s = tr.ReadLine()) != null)
               {
                  ParseLineXml(s);
               }
               readFields();
               b_modified = true;
               updateFilename();
            }
            else
            {
               MessageBox.Show("File: " + fileRDS + " does not exist.");
            }
         }
         catch (Exception e)
         {
               MessageBox.Show("readRDS: " + e.Message);
         }
      }
      private void ParseLineXml(string s)
      {
         s.TrimStart();
         string key, value;
         //XmlElement N;

         if (s.Length == 0)
         {
               return;
         }

         switch (s.Substring(0, 1))
         {
         case "[":
               // this is a section
               // trim the first and last characters
               s = s.TrimStart('[');
               s = s.TrimEnd(']');
               break;
         case ";":
               // new comment
               break;
         default:
               // split the string on the "=" sign, if present
               if (s.IndexOf('=') > 0)
               {
                  String[] parts = s.Split('=');
                  key = parts[0].Trim().ToLower();
                  value = parts[1].Trim();
               }
               else
               {
                  key = s;
                  value = "";
               }
               if (key == "data")
               {
                  string[] values = value.Split(',');
                  int blockSize = 7;
                  XElement xData = new XElement("data");

                  for (int i = blockSize; i < values.GetUpperBound(0); i += blockSize)
                  {
                     int index = ((i/blockSize)-1);

                     XElement xItem = new XElement(
                        "item",
                        new XElement("index", index.ToString()),
                        new XElement("type", values[i]),
                        new XElement("calc", (values[i + 1]=="0")?"False":"True"),
                        new XElement("start", values[i + 4]),
                        new XElement("len", values[i + 5]),
                        new XElement("text", values[i + 6]),
                        new XElement("add", values[i + 2]),
                        new XElement("outlen", values[i + 3])
                     );
                     xData.Add(xItem);
                  }
                  (from el in _doc.Descendants("data") select el).First().Remove();
                  _doc.Element("root").Add(xData); 
               }
               else
               {
                  updateSettingNode(key, value);
               }
               break;
         }
      }
      #endregion

      #region GridViewManipulation
      private void gvData_SelectionChanged(object sender, EventArgs e)
      {
         try
         {
               upDownBtnEnable();

               lblIndex.Text = gvData.SelectedRows[0].Cells["index"].Value.ToString();
               txtLen.Text = gvData.SelectedRows[0].Cells["len"].Value.ToString();
               txtStart.Text = gvData.SelectedRows[0].Cells["start"].Value.ToString();
               txtText.Text = gvData.SelectedRows[0].Cells["text"].Value.ToString();
               txtAdd.Text = gvData.SelectedRows[0].Cells["add"].Value.ToString();
               txtOutLen.Text = gvData.SelectedRows[0].Cells["outlen"].Value.ToString();
               if (gvData.SelectedRows[0].Cells["calc"].Value.ToString() == "True") { chkCalc.Checked = true; } else { chkCalc.Checked = false; }
               switch (gvData.SelectedRows[0].Cells["type"].Value.ToString().ToLower())
               {
                  case "left":
                     rbLeft.Checked = true;
                     break;
                  case "middle":
                     rbMiddle.Checked = true;
                     break;
                  case "right":
                     rbRight.Checked = true;
                     break;
                  default:
                     rbText.Checked = true;
                     break;
               }
         }
         catch
         {
         }
      }
      private void upDownBtnEnable()
      {
         int selIndex;

         try
         {
            selIndex = int.Parse(gvData.SelectedRows[0].Cells["index"].Value.ToString());
         } catch 
         {
            selIndex = 0;
         }
         if (selIndex > 0) { btnUp.Enabled = true; } else { btnUp.Enabled = false; }
         if (selIndex < gvData.Rows.Count - 1) { btnDown.Enabled = true; } else { btnDown.Enabled = false; }
      }

      private void btnDown_MouseClick(object sender, MouseEventArgs e)
      {
         moveSelectedItem(direction.down);
      }
      private void btnUp_Click(object sender, EventArgs e)
      {
         moveSelectedItem(direction.up);
      }
      private enum direction { up = -1, down = 1 }
      private void moveSelectedItem(direction Move)
      {
         int frstIndex = int.Parse(lblIndex.Text);
         int scndIndex = frstIndex + (int)Move;

         XElement frst = (
               from el in _doc.Descendants("data").Descendants("item")
               where (string)el.Element("index") == frstIndex.ToString()
               select el).First();
         XElement scnd = (
               from el in _doc.Descendants("data").Descendants("item")
               where (string)el.Element("index") == scndIndex.ToString()
               select el).First();
         frst.Descendants("index").First().SetValue("tmp");
         scnd.Descendants("index").First().SetValue(frstIndex.ToString());
         frst.Descendants("index").First().SetValue(scndIndex.ToString());
         b_modified = true;
         updateFilename();
         getData();
         for (int i = 0; i < gvData.Rows.Count; i++)
         {
               gvData.Rows[i].Selected = (i == scndIndex);
         }
      }
      private void rbLeft_CheckedChanged(object sender, EventArgs e)
      {
         FieldEnables();
      }
      private void rbMiddle_CheckedChanged(object sender, EventArgs e)
      {
         FieldEnables();
      }
      private void rbRight_CheckedChanged(object sender, EventArgs e)
      {
         FieldEnables();
      }
      private void rbText_CheckedChanged(object sender, EventArgs e)
      {
         FieldEnables();
      }
      private void chkCalc_CheckedChanged(object sender, EventArgs e)
      {
         FieldEnables();
      }
      private void FieldEnables()
      {
         txtStart.Enabled = rbMiddle.Checked;
         txtLen.Enabled = (rbLeft.Checked || rbMiddle.Checked || rbRight.Checked);
         txtText.Enabled = rbText.Checked;
         chkCalc.Enabled = (rbLeft.Checked || rbMiddle.Checked || rbRight.Checked);
         txtAdd.Enabled = chkCalc.Enabled && chkCalc.Checked;
         txtOutLen.Enabled = chkCalc.Enabled && chkCalc.Checked;
      }
      private void btnUpdate_Click(object sender, EventArgs e)
      {
         string myIndex = lblIndex.Text;
         XElement xItem = new XElement(
                           "item",
                              new XElement("index", myIndex),
                              new XElement("type", getDataType()),
                              new XElement("calc", chkCalc.Checked.ToString()),
                              new XElement("start", txtStart.Text),
                              new XElement("len", txtLen.Text),
                              new XElement("text", txtText.Text),
                              new XElement("add", txtAdd.Text),
                              new XElement("outlen", txtOutLen.Text)
                           );
         XElement frst = (
               from el in _doc.Descendants("data").Descendants("item")
               where (string)el.Element("index") == myIndex
               select el).First();
         frst.Remove();
         _doc.Descendants("data").First().Add(xItem);
         b_modified = true;
         getData();
         updateFilename();
         for (int i = 0; i < gvData.Rows.Count; i++)
         {
               gvData.Rows[i].Selected = (i == int.Parse(myIndex));
         }
      }
      private void btnAddNew_Click(object sender, EventArgs e)
      {
         string newIndex = gvData.Rows.Count.ToString();
         XElement xItem = new XElement(
                           "item",
                              new XElement("index", newIndex),
                              new XElement("type", "text"),
                              new XElement("calc", "False"),
                              new XElement("start"),
                              new XElement("len"),
                              new XElement("text"),
                              new XElement("add"),
                              new XElement("outlen")
                           );
         _doc.Descendants("data").First().Add(xItem);
         b_modified = true;
         getData();
         updateFilename();
         for (int i = 0; i < gvData.Rows.Count; i++)
         {
               gvData.Rows[i].Selected = (i == int.Parse(newIndex));
         }
      }
      private void btnRemove_Click(object sender, EventArgs e)
      {
         string myIndex = lblIndex.Text;
         XElement frst = (
               from el in _doc.Descendants("data").Descendants("item")
               where (string)el.Element("index") == myIndex
               select el).First();
         frst.Remove();
         foreach (XElement xItem in
               (from x in _doc.Descendants("data").Elements()
               where (int)x.Element("index") > int.Parse(myIndex)
               select x
               )
         )
         {
               xItem.Element("index").SetValue(int.Parse(xItem.Element("index").Value)-1);
         }
         b_modified = true;
         updateFilename();
         getData();
      }
      private string getDataType()
      {
         string myDataType = "text";
         if (rbLeft.Checked == true) myDataType = "left";
         if (rbMiddle.Checked == true) myDataType = "middle";
         if (rbRight.Checked == true) myDataType = "right";
         return myDataType;
      }
      #endregion

      #region ProcessRelated
      private bool ValidateFields()
      {
         if (txtSource.Text.Trim() == "")
         {
               MessageBox.Show("You must specify a source directory.");
               return false;
         }
         if (txtDest.Text.Trim() == "")
         {
               MessageBox.Show("You must specify a destination directory.");
               return false;
         }
         if (txtInTemplate.Text.Trim() == "")
         {
               MessageBox.Show("You must specify an input template regular expression.");
               return false;
         }
         if (gvData.Rows.Count < 1)
         {
               MessageBox.Show("You must specify output file naming rules.");
               return false;
         }

         return true;
      }
      private void btnProcess_Click(object sender, EventArgs e)
      {
         Cursor.Current = Cursors.WaitCursor;
         myProcess();
         Cursor.Current = Cursors.Arrow;
         prgProgress.Visible = false;
      }

      int myProcess()
      {
         int iICnt=0, iOCnt=0;

         if (ValidateFields() == false) return -1;
         Image img = null;
         Image imgAdd = null;
         ImageCodecInfo codec = GetEncoderInfo("image/tiff");
         EncoderParameters imgPrms = new EncoderParameters(2);
         EncoderParameter mf = new EncoderParameter(Encoder.SaveFlag, (long)EncoderValue.MultiFrame);
         EncoderParameter fdp = new EncoderParameter(Encoder.SaveFlag, (long)EncoderValue.FrameDimensionPage);
         EncoderParameter flush = new EncoderParameter(Encoder.SaveFlag, (long)EncoderValue.Flush);
         //imgPrms.Param[0] = new EncoderParameter(Encoder.ColorDepth, 1);
         imgPrms.Param[0] = new EncoderParameter(Encoder.Compression, (long)EncoderValue.CompressionCCITT4);
         imgPrms.Param[1] = mf;

         float scale = 1;
         Single myWidth;
         Single myHeight;
         ceTe.DynamicPDF.Document document = new ceTe.DynamicPDF.Document();

         FileInfo src = new FileInfo(txtSource.Text);
         FileInfo dst = new FileInfo(txtDest.Text);
         if (!src.Directory.Exists)
         {
               MessageBox.Show("Directory " + txtSource.Text + " is not accessible or does not exist.");
               return -1;
         }
         if (!dst.Directory.Exists)
         {
               DialogResult result = MessageBox.Show("Directory " + txtDest.Text + " is not accessible or does not exist. Would you like to create it?", "Create?", MessageBoxButtons.YesNo);
               if (result == DialogResult.No) return -1;
               dst.Directory.Create();
         }
         ArrayList masterList = (ArrayList)DescendDirs(txtSource.Text, txtInTemplate.Text);
         prgProgress.Maximum = masterList.Count;
         prgProgress.Value = 0;
         prgProgress.Visible = true;
         masterList.Sort();
         string prevDestFile = "";
         string destDir = "";
         string prevDestDir = "";
         string destFile = "";
         foreach (string fileName in masterList)
         {
            iICnt++;
            destFile = Path.Combine(txtDest.Text, makeFilename(fileName));
            destDir = new FileInfo(destFile).Directory.ToString();
            if (destDir != prevDestDir) 
               CheckPath(new FileInfo(destFile).Directory);

            if (chkMerge.Checked == true)    // If merging
            {
               if (chkPDF.Checked == true)   // PDF output format
               {
                  //Get image for scaling
                  img = Image.FromFile(fileName);
                  myWidth = (img.Width / img.HorizontalResolution) * 72;
                  myHeight = (img.Height / img.VerticalResolution) * 72;
                  scale = 72 / img.HorizontalResolution;
                  img.Dispose();

                  int i;
                  try
                  {
                     ceTe.DynamicPDF.Imaging.TiffFile myTiff = new ceTe.DynamicPDF.Imaging.TiffFile(fileName);
                     for (i = 0; i < myTiff.Images.Count; i++)
                     {
                        ceTe.DynamicPDF.Page myPage = new ceTe.DynamicPDF.Page(myWidth, myHeight, 0);
                        ceTe.DynamicPDF.PageElements.Image map = new ceTe.DynamicPDF.PageElements.Image(myTiff.Images[i], 0, 0, scale);
                        if (chkNoRotate.Checked == false) myPage.Rotate = 90;
                        myPage.Elements.Add(map);

                        if (destFile != prevDestFile)
                        {
                           if (prevDestFile != "") // First page
                           {
                              //Save previous document
                              document.Draw(prevDestFile);
                              //Create new document
                              document = new ceTe.DynamicPDF.Document();
                           }
                           prevDestFile = destFile;
                        }
                        document.Pages.Add(myPage);
                     }
                     iOCnt++;
                  }
                  catch (Exception e)
                  {
                     LogMsg("***** Fail converting: " + fileName);
                     LogMsg("Error: " + e.Message);
                  }
               }
               else // Multi page tiff output format
               {
                  if (destFile == prevDestFile)
                  {
                     imgAdd = Image.FromFile(fileName);
                     img.SaveAdd(imgAdd, imgPrms);
                     imgAdd.Dispose();
                  }
                  else
                  {
                     if (prevDestFile != "") // First page, just copy the image over.
                     {
                        imgPrms.Param[1] = flush;
                        img.SaveAdd(imgPrms);
                        img.Dispose();
                     }
                     img = Image.FromFile(fileName);
                     imgPrms.Param[1] = mf;
                     img.Save(destFile, codec, imgPrms);
                     imgPrms.Param[1] = fdp;
                     iOCnt++;
                  }
               }
            }
            else
            {
               FileInfo dest = new FileInfo(destFile);
               if (dest.Exists) dest.Delete(); // Delete dest file to minimize errors.
               File.Copy(fileName, destFile);
            }
            prevDestFile = destFile; // remember previous dest file for file merging.
            prevDestDir = destDir;
            if (chkDelOrig.Checked == true) File.Delete(fileName); // Delete original file if checked.
            Application.DoEvents();
            prgProgress.Value += 1;
            
            LogMsg("==> " + fileName);
         }
         if (chkMerge.Checked == true) // If merging
         {
               if (chkPDF.Checked == true) // PDF output format
               {
                  document.Draw(destFile);
               }
               else // Multi page tiff output format
               {
                  imgPrms.Param[1] = flush;
                  img.SaveAdd(imgPrms);
                  img.Dispose();
               }
         }

         LogMsg("Number of images processed: " + iICnt);
         LogMsg("Number of images output:    " + iOCnt);

         return iOCnt;
      }

      ImageCodecInfo GetEncoderInfo(string mimeType)
      {
         foreach (ImageCodecInfo ice in ImageCodecInfo.GetImageEncoders())
         {
               if (ice.MimeType.Equals(mimeType)) return ice;
         }
         return null;
      }

      void CheckPath(DirectoryInfo di)
      {
         if (di.Exists == false)
         {
               CheckPath(di.Parent);
               di.Create();
         }
      }
      string makeFilename(string inFullFile)
      {
         string newFile = "";
         string dynFile = inFullFile.Substring(txtSource.Text.TrimEnd('\\').Length + 1);

         foreach (DataGridViewRow myRow in gvData.Rows)
         {
               string sTemp;
               switch (myRow.Cells["type"].Value.ToString().ToLower())
               {
                  case "left":
                     sTemp = dynFile.Substring(0, int.Parse(myRow.Cells["len"].Value.ToString()));
                     break;
                  case "right":
                     sTemp = dynFile.Substring(dynFile.Length - int.Parse(myRow.Cells["len"].Value.ToString()), int.Parse(myRow.Cells["len"].Value.ToString()));
                     break;
                  case "middle":
                     sTemp = dynFile.Substring(int.Parse(myRow.Cells["start"].Value.ToString()) - 1, int.Parse(myRow.Cells["len"].Value.ToString()));
                     break;
                  case "text":
                     sTemp = myRow.Cells["text"].Value.ToString();
                     break;
                  default:
                     sTemp = "";
                     break;
               }
               newFile += FieldCalc(sTemp, 
                  myRow.Cells["calc"].Value.ToString(), 
                  myRow.Cells["add"].Value.ToString(), 
                  myRow.Cells["outlen"].Value.ToString());
         }
         return newFile;
      }
      string FieldCalc(string inString, string Calc, string Add, string OutLen)
      {
         string newString = inString;
         if (Calc == "True")
         {
               int iAdd = (Add == "") ? 0 : int.Parse(Add);
               int iOut = (OutLen == "") ? 0 : int.Parse(OutLen);
               int iNewVal = int.Parse(inString) + iAdd;
               if (iNewVal.ToString().Length >= int.Parse(OutLen))
               {
                  string tmp = iNewVal.ToString();
                  newString = tmp.Substring(tmp.Length - int.Parse(OutLen));
               }
               else
               {
                  string sFormat = "0";
                  for (int i = 1; i < int.Parse(OutLen); i++)
                  {
                     sFormat += "0";
                  }
                  newString = iNewVal.ToString(sFormat);
               }
         }
         return newString;
      }
      ArrayList DescendDirs(string parent, string regExp)
      {
         string[] aRegExp;
         ArrayList foundDirs = new ArrayList();
         ArrayList finalDirs = new ArrayList();
         string sTemp = "";

         aRegExp = regExp.Split(new string[] {"\\\\"},StringSplitOptions.None);
         if (aRegExp.GetUpperBound(0) > 0) // handle sub directories
         {
               for (int i = 1; i <= aRegExp.GetUpperBound(0);i++) // Grab the regexp but not including the current level.
               {
                  sTemp = sTemp + ((sTemp=="")?"":"\\\\") + aRegExp[i];
               }
               DirectoryInfo thisDir = new DirectoryInfo(parent);
               DirectoryInfo[] subDirs = thisDir.GetDirectories();
               Regex testExp = new Regex(aRegExp[0], RegexOptions.IgnoreCase);
               foreach (DirectoryInfo diNext in subDirs)
               {
                  Match m = testExp.Match(diNext.Name);
                  if (m.Success) foundDirs.Add(diNext);
               }
               foreach (DirectoryInfo diNext in foundDirs)
               {
                  ArrayList tmpDirList = (ArrayList)DescendDirs(diNext.FullName, sTemp);
                  finalDirs.AddRange(tmpDirList);
               }
         }
         else // at bottom level
         {
               DirectoryInfo thisDir = new DirectoryInfo(parent);
               FileInfo[] theseFiles = thisDir.GetFiles();
               Regex testExp = new Regex(aRegExp[0], RegexOptions.IgnoreCase);
               foreach (FileInfo fiNext in theseFiles)
               {
                  Match m = testExp.Match(fiNext.Name);
                  if (m.Success) finalDirs.Add(fiNext.FullName);
               }
         }
         return finalDirs;
      }
      #endregion
   }
}
